use futures::{Future, executor::LocalPool, task::LocalSpawnExt};
use std::time::Duration;
use async_std::task;
use rand::{thread_rng, Rng};
use tracing::{instrument, Level};
use tracing_subscriber;

fn rand_sleep() -> impl Future {
    let mut rng = thread_rng();
    let sleep_for = Duration::from_millis(rng.gen_range(0, 100));
    task::sleep(sleep_for)
}

#[instrument]
async fn foo() {
    tracing::info!("Foo is doing some work");

    rand_sleep().await;

    tracing::info!("Foo is finished!");
}

#[instrument]
async fn bar() {
    tracing::info!("Bar is doing some other work");

    rand_sleep().await;

    tracing::error!("I just failed!")
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .init();

    let mut pool = LocalPool::new();

    pool.spawner()
        .spawn_local(foo())
        .expect("failed");

    pool.spawner()
        .spawn_local(bar())
        .expect("failed");

    pool.run();
}
