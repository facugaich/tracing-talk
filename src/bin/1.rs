use log;
use stderrlog;

fn foo() {
    log::info!("Foo is doing some work");
}

fn bar() {
    log::info!("Bar is doing some other work");

    log::error!("I just failed!")
}

fn main() {
    stderrlog::new()
        .module(module_path!())
        .verbosity(3)
        .init()
        .unwrap();

    log::info!("Entering Foo");
    foo();

    log::info!("Entering Bar");
    bar();

}
