use tracing::{span, instrument, Level};
use tracing_subscriber;

fn foo() {
    tracing::info!("Foo is doing some work");
}

#[instrument]
fn bar() {
    tracing::info!("Bar is doing some other work");

    tracing::error!("I just failed!")
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .init();

    span!(Level::INFO, "Foo span").in_scope(|| {
        foo();
    });

    bar();
}
