use futures::{Future, executor::LocalPool, task::LocalSpawnExt};
use std::time::Duration;
use async_std::task;
use rand::{thread_rng, Rng};
use tracing::{
    instrument,
    Subscriber,
    Event,
    Id,
    field::{Visit, Field},
    span::Attributes,
};
use tracing_subscriber::{
    Layer,
    layer::{Context, SubscriberExt},
    registry::{Registry, LookupSpan},
    util::SubscriberInitExt
};
use std::collections::VecDeque;
use crossbeam_queue::SegQueue;
use std::fmt;

pub struct BufferLayer<'a> {
    pub flushables: &'a SegQueue<VecDeque<String>>,
}

fn rand_sleep() -> impl Future {
    let mut rng = thread_rng();
    let sleep_for = Duration::from_millis(rng.gen_range(0, 100));
    task::sleep(sleep_for)
}

#[instrument]
async fn foo() {
    tracing::info!("Foo is doing some work");

    rand_sleep().await;

    tracing::info!("Foo is finished!");
}

#[instrument]
async fn bar() {
    tracing::info!("Bar is doing some other work");

    rand_sleep().await;

    tracing::error!("I just failed!")
}

fn main() {

    let segque: &'static SegQueue<_> = Box::leak(Box::new(SegQueue::new()));

    let layer = BufferLayer {
        flushables: segque,
    };

    Registry::default()
        .with(layer)
        .init();

    let mut pool = LocalPool::new();

    pool.spawner()
        .spawn_local(foo())
        .expect("failed");

    pool.spawner()
        .spawn_local(bar())
        .expect("failed");

    pool.run();

    BufferLayer::flush(segque);
}

impl BufferLayer<'_> {
    fn flush(flushables: &SegQueue<VecDeque<String>>) {

        while !flushables.is_empty() {
            if let Ok(mut inner) = flushables.pop() {
                let mut s = inner.pop_front();
                while let Some(x) = s {
                    println!("{}", x);
                    s = inner.pop_front();
                }
            }
        }

    }
}

pub struct SegQueueVisitor<'a> {
    pub queue: &'a SegQueue<String>,
}

impl<'a> Visit for SegQueueVisitor<'a> {
    fn record_debug(&mut self, field: &Field, value: &dyn fmt::Debug) {
        self.queue.push(format!("{} = {:?}", field.name(), value));
    }
}

impl<S> Layer<S> for BufferLayer<'static>
where
    S: Subscriber + for<'a> LookupSpan<'a>,
{

    fn new_span(&self, _attrs: &Attributes, id: &Id, ctx: Context<S>) {
        let data = ctx.span(id).unwrap();
        data.extensions_mut().insert(SegQueue::<String>::new());
    }

    fn on_event(&self, event: &Event, ctx: Context<S>) {
        let data = ctx.lookup_current().unwrap();
        let ext = data.extensions();
        let mut visitor = SegQueueVisitor {
            queue: ext.get().unwrap()
        };
        event.record(&mut visitor);
    }

    fn on_close(&self, id: Id, ctx: Context<S>) {
        let data = ctx.span(&id).unwrap();
        let ext = data.extensions();
        let q: &SegQueue<String> = ext.get().unwrap();

        let mut out = VecDeque::new();
        out.push_back(format!("-- {}", data.name()));
        while !q.is_empty() {
            if let Ok(v) = q.pop() {
                out.push_back(format!("| {}", v));
            }
        }
        out.push_back("--".to_string());
        self.flushables.push(out);
    }

}
