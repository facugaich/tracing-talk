use log;
use stderrlog;
use futures::{Future, executor::LocalPool, task::LocalSpawnExt};
use std::time::Duration;
use async_std::task;
use rand::{thread_rng, Rng};

fn rand_sleep() -> impl Future {
    let mut rng = thread_rng();
    let sleep_for = Duration::from_millis(rng.gen_range(0, 100));
    task::sleep(sleep_for)
}

async fn foo() {
    log::info!("Foo is doing some work");

    rand_sleep().await;

    log::info!("Foo is finished!");
}

async fn bar() {
    log::info!("Bar is doing some other work");

    rand_sleep().await;

    log::error!("I just failed!")
}

fn main() {
    stderrlog::new()
        .module(module_path!())
        .verbosity(3)
        .init()
        .unwrap();

    let mut pool = LocalPool::new();

    log::info!("Scheduling Foo");
    pool.spawner()
        .spawn_local(foo())
        .expect("failed");

    log::info!("Scheduling Bar");
    pool.spawner()
        .spawn_local(bar())
        .expect("failed");

    pool.run();
}
