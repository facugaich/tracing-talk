use tracing::{instrument, Level};
use tracing_subscriber;

#[instrument]
fn foo() {
    tracing::info!("Foo is doing some work");

    bar();
}

#[instrument]
fn bar() {
    tracing::info!("Bar is doing some other work");

    tracing::error!("I just failed!")
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .init();

    foo();
}
