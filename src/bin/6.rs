use futures::{Future, executor::LocalPool, task::LocalSpawnExt};
use std::time::Duration;
use async_std::task;
use rand::{thread_rng, Rng};
use tracing_futures::Instrument;
use tracing::{span, Level};
use tracing_subscriber;

fn rand_sleep() -> impl Future {
    let mut rng = thread_rng();
    let sleep_for = Duration::from_millis(rng.gen_range(0, 100));
    task::sleep(sleep_for)
}

async fn foo() {
    tracing::info!("Foo is doing some work");

    rand_sleep().await;

    tracing::info!("Foo is finished!");
}

async fn bar() {
    tracing::info!("Bar is doing some other work");

    rand_sleep().await;

    tracing::error!("I just failed!")
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .init();

    let mut pool = LocalPool::new();

    let foo_span = span!(Level::INFO, "Foo span");
    let foo_instrumented = foo().instrument(foo_span);
    pool.spawner()
        .spawn_local(foo_instrumented)
        .expect("failed");

    let bar_span = span!(Level::INFO, "Bar span");
    let bar_instrumented = bar().instrument(bar_span);
    pool.spawner()
        .spawn_local(bar_instrumented)
        .expect("failed");

    pool.run();
}
