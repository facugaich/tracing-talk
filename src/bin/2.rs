use tracing::{span, Level};
use tracing_subscriber;

fn foo() {
    tracing::info!("Foo is doing some work");
}

fn bar() {
    tracing::info!("Bar is doing some other work");

    tracing::error!("I just failed!")
}

fn main() {
    tracing_subscriber::fmt()
        .with_max_level(Level::INFO)
        .init();

    let foo_span = span!(Level::INFO, "Foo span");
    let guard = foo_span.enter();

    foo();

    drop(guard);

    {
        let bar_span = span!(Level::INFO, "Bar span");
        let _guard = bar_span.enter();

        bar();
    }
}
